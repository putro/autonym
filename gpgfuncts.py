import sys
import re
import gnupg
import getpass
from config import config
from utils import pressKey


try:
    global gpg
    gpg = gnupg.GPG()
except:
    print("Error, problems with your keyring directory, exiting")
    sys.exit(0)


def createKey(email, name, comment="", key_type="RSA", key_length=4096):
    """ Create a gpg key """
    while True:
        pwd = getpass.getpass("insert the password: ")
        pwd2 = getpass.getpass("type again the password: ")
        if pwd == pwd2:
            break
    print("wait, I'm generating the key, move the mouse, " \
          "press keys, it could take some times......\n")
    input_data = gpg.gen_key_input(name_real=name, name_email=email,
                                   name_comment="", passphrase=pwd, key_type=key_type,
                                   key_length=key_length, )
    key = gpg.gen_key(input_data)


def selectKey(email, secret=False):
    """ Select the key fingerprint of an email address"""
    keys = gpg.list_keys(secret=secret)
    key = False
    for k in keys:
        counter = 0
        # if keys have multiple uids, scan through all uids
        for u in k["uids"]:
            # stop at first match
            e = k["uids"][counter]
            counter += 1
            expr = re.compile("%s" % email)
            m = expr.search(e)
            """ if match found, select key """
            if m:
                """check if key is valid"""
                if encrypt("test", k["fingerprint"],
                           test=True).status == "encryption ok":
                    key = True
                    config['last_message'] = "OK - key choosed: keyID=%s" \
                                             % getKeyID(k["fingerprint"])
                    break

                else:
                    config['last_message'] = "ERROR - invalid key for " \
                        "recipient: %s" % u
                    key = False
        if key:
            break
    if not key:
        config['last_message'] = "ERROR, public key for %s NOT found" % email
        print("ERROR, public key for %s NOT found" % email)
        pressKey()
        return False
    return k["fingerprint"]


def listSecKeys():
    """ Print list of secret keys in keyring"""
    seckeys = gpg.list_keys(secret=True)
    counter = 0
    for k in seckeys:
        counter += 1
        print("%s - %s - %s" % (counter, k["uids"][0], k["keyid"]))
        print([x for x in k["uids"]])
    return seckeys


def selectSecKey(email):
    """ Select the key fingerprint of an email address"""
    global last_message
    seckeys = gpg.list_keys(True)
    key = False
    for k in seckeys:
        counter = 0
        """if keys have multiple uids, scan through all uids"""
        for u in k["uids"]:
            """stop at first match"""
            e = k["uids"][counter]
            counter += 1
            expr = re.compile("%s" % email)
            m = expr.search(e)
            """ if match found, select key """
            if m:
                #print "key fingerprint for %s: %s" % (email, k["fingerprint"])
                config['nym_fp'] = k["fingerprint"]
                config['nym_keyid'] = getKeyID(k["fingerprint"])
                key = True
                last_message = "OK - secret key choosed: keyID=%s" \
                    % config['nym_keyid']
                break
        if key:
            break
    if not key:
        config['last_message'] = "ERROR, secret key for %s NOT found" % email
        return False
    return k["fingerprint"]


def getKeyID(fp):
    """ Get keyid from fingeprint """
    # works only with v4 keys
    return fp[-8:]


def exportPubKey(id):
    """ Export public key
    id can be the fingerprint """
    ascii_armored_public_keys = stripVersion(str(gpg.export_keys(id)))
    return ascii_armored_public_keys


def defineSecKey():
    """ Select which secret key to use """
    ls = listSecKeys()
    numkeys = int(len(ls))
    print("there are %s keys" % numkeys)
    if numkeys == 0:
        print("going to generate a new key...")
        createKey()
        listSecKeys()

    while True:
        a = input("enter key number (and press enter): ")
        if a.isdigit():
            a = int(a)
            if a < 1:
                print("you can't enter a key number lower than 1")
                continue
            elif a > numkeys:
                print("there are only %d keys, select key number again" \
                      % numkeys)
                continue
            break
    a -= 1
    return ls[a]


def deleteKey(fp):
    """ Delete secret and public key """
    global last_message
    seckey = gpg.list_keys(secret=True)
    str(gpg.delete_keys(fp, True))
    str(gpg.delete_keys(fp))
    last_message = "OK - key with fingerprint %s deleted" % fp


def stripVersion(pgptext):
    """Version strings in PGP blocks can weaken anonymity by partitioning users
    into subsets.  This function replaces the Version string with 'N/A'."""
    newtext = re.sub('(?m)^Version: .*', 'Version: N/A', pgptext)
    return newtext


def encrypt(message, recipient, sign=False, passphrase=False, test=False):
    """ Encrypt a message for a recipient """
    encrypted_ascii_data = gpg.encrypt(message, recipient, always_trust=True,
                                       sign=sign, passphrase=passphrase)
    if not test:
        if encrypted_ascii_data.status != "encryption ok":
            config['last_message'] = "ERROR " + encrypted_ascii_data.status + \
                                     " (recipient: %s)" % recipient
            return "ERROR"
        else:
            return stripVersion(str(encrypted_ascii_data))
    else:
        return encrypted_ascii_data
