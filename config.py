import os
import sys
import checks
from configobj import ConfigObj, ConfigObjError

class ConfigError(Exception):
    pass


configfile = os.path.dirname(sys.argv[0]) + "/config.ini"

try:
    config = ConfigObj(configfile, file_error=True)
except (ConfigObjError, IOError) as e:
    print('Could not read "%s": %s' % (configfile, e))
    sys.exit(-1)


nyms = config['nym']




def writeConfig():
    config.write()

def checkSection(section, config=config):
    if section in listSection(config):
        return True

def listSection(config):
    return config.keys()

def configSection(nym, subject="", subj_type="", passphrase=""):
    config['nym'][nym] = {}
    config['nym'][nym]['subject'] = subject
    config['nym'][nym]['subj_type'] = subj_type
    config['nym'][nym]['passphrase'] = passphrase


def delSection(nym):
    del config['nym'][nym]

def checkNymConfig(nym):
    return nyms.has_key(nym)

def nymList(nyms):
    nymlist = []
    for n in nyms.keys():
        nymlist.append(n)
    return nymlist

def printNymList():
    nymlist = nymList()
    counter = 0
    for n in nymlist:
        print(counter, "-", n)
        counter += 1
    return counter


