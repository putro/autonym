import os
import sys
import re
import getpass


class InputError(Exception):
    pass


def download(file, config):
    """ Download remailer stats file """
    print("downloading %s....... please wait" % file)
    import urllib.request
    url = config['stats']['stats_url'] + file
    try:
        urllib.request.urlretrieve(url, os.path.dirname(sys.argv[0]) + "/" + file)
    except Exception as e:
        print('Error downloading file.')
        print('Reason: ', e)
        pressKey()


def pressKey():
    """ wait user to press RETURN """
    input("press RETURN to continue\n")


def validateEmail(email):
    """ check if the recipient is a valid email address"""
    if re.match(r'[\w\-][\w\-\.]*@[\w\-][\w\-\.]+[a-zA-Z]{1,4}', email):
        return True
    else:
        return False


def validateChoice(choice, list):
    try:
        i = (int(choice))
    except ValueError:
        print("Invalid input, enter a number....")
        return False
    if i < 1:
        print("enter a number greater than 1....")
        return False
    if i > len(list):
        print("error, there are only %s choices" % len(list))
        return False
    else:
        return True


def askPassphrase():
    """ Ask passphrase for nym secret key if not set in config"""
    pwd = getpass.getpass("insert passphrase for nym secret key: ")
    return pwd


def askSomething(question):
    usr_input = input(question)
    return usr_input


def askYesNo(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": True,   "y": True,  "ye": True,
             "no": False,     "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def printList(list):
    counter = 1
    for i in list:
        print(counter, "-", i)
        counter += 1


def chooseList(list):
    printList(list)
    usr_input = ''
    while usr_input not in range(1, len(list) + 1):
        try:
            usr_input = input("Enter your choice (1-%s): " % len(list))
        except:
            print("\nError - you have to enter a number !!!!")
    return list[usr_input - 1]


def selectServer(config):
    servers = config['options']['nymservers']
    for idx, val in enumerate(servers):
        print(idx + 1, val)

    while True:
        choice = input("Enter the nymserver you want to use: ")
        if validateChoice(choice, servers):
            break

    server = servers[int(choice) - 1]
    return server


def chooseSubjType(config):
    subject_types = config['options']['subj_types']
    for idx, val in enumerate(subject_types):
        print(idx + 1, "-", val)

    while True:
        choice = input("Enter what kind of subject you want: ")
        if validateChoice(choice, subject_types):
            break

    subj_type = subject_types[int(choice) - 1]
    return subj_type


def getDomain(email):
    domain = email.split("@")[1]
    return domain


def isGpg(message):
    m = re.compile('^-----BEGIN PGP MESSAGE-----')
    line = message.split("\n", 1)[0]
    print(line)
    pressKey()
    if m.match(line):
        return True
    else:
        return False


def isAscii(text):
    return all(ord(c) < 128 for c in text)


def editMessage():
    import tempfile, os
    from subprocess import call

    EDITOR = os.environ.get('EDITOR', 'vim')

    initial_message = ""   # if you want to set up the file somehow

    with tempfile.NamedTemporaryFile(suffix=".tmp") as tempfile:
        tempfile.write(initial_message)
        tempfile.flush()
        call([EDITOR, tempfile.name])

        f = open(tempfile.name, "r")
        data = f.read()
        f.close()

    return data


def X_is_running():
    from subprocess import Popen, PIPE
    p = Popen(["xset", "-q"], stdout=PIPE, stderr=PIPE)
    p.communicate()
    return p.returncode == 0
