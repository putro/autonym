#!/usr/bin/env python3

import os
import sys
from utils import download
import subprocess


class MixError(Exception):
    pass


def checkAscii(file):
    """ Check if the a file is pure ascii """
    import codecs
    bad = 0
    f = codecs.open(file, encoding='ascii')
    lines = open(file).readlines()
    for i in range(0, len(lines)):
        try:
            l = f.readline()
        except:
            num = i + 1
            print("config file problem (%s): line %d contains non-ascii character, fix it" % (file, num))
            bad = 1
            break
    f.close()
    return bad


def OptionsCheck(config):
    """ performs some checks """
    try:
        f = open(os.path.dirname(sys.argv[0]) + "/check", "w")
    except Exception as e:
        print(e)
        print("Error: you cannot write here, fix permission")
        sys.exit()
    else:
        f.close()
        os.remove(os.path.dirname(sys.argv[0]) + "/check")

    try:
        download(config['stats']['stats_mlist'], config)
    except Exception as e:
        print(e)
        print("Error: cannot download fresh remailer stats")
        sys.exit()

    try:
        mix = subprocess.Popen(config['options']['mixmaster'],
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError:     # usally means that mixmaster could not be executed
        raise MixError('Could not find mixmaster binary.')

    return True
